class MainMenu extends Phaser.State {

    create() {
        var buttonWidth = this.game.world.width / 2;
        var buttonHeight = buttonWidth / 3.125;

        this.createButton(
            this.game.world.centerX - buttonWidth / 2,
            this.game.world.centerY,
            'start',
            this.startGame,
            buttonWidth,
            buttonHeight);

        var logoWidth = this.game.world.width * 0.8;
        var logoHeight = logoWidth / 2.62;

        var logo = this.game.add.image(this.game.world.width * 0.1, this.game.world.centerY - 1.5 * buttonHeight - logoHeight, 'logo');
        logo.width = logoWidth;
        logo.height = logoHeight;

        const settings = this.game.cache.getJSON('settings');
        this.game.stage.backgroundColor = settings.game.backgroundColor;
    }

    startGame() {
        this.game.state.start("Game");
    }

    createButton(x, y, image, command, width, height) {
        var button = this.add.button(x, y, image, command, this, 1, 0, 2);
        button.width = width;
        button.height = height;
    }
}
export default MainMenu;