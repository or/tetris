class Piece {
    constructor(pieceTemplates) {
        this.rotation = 0;

        var json = pieceTemplates;

        var random = Math.floor(Math.random() * json.shapes.length);
        this.blockShape = json.shapes[random].rotations;
        this.color = json.shapes[random].color;

        this.location = JSON.parse(JSON.stringify(this.blockShape[this.rotation]));

        this.x = 0;
        this.y = 0;
    }

    rotate() {
        this.rotation++;
        if (this.rotation === this.blockShape.length) {
            this.rotation = 0;
        }
        this.location = JSON.parse(JSON.stringify(this.blockShape[this.rotation]));
        this.moveTo(this.x, this.y);
    }

    rotateBack() {
        try {
            this.rotation--;
            if (this.rotation < 0) {
                this.rotation = this.blockShape.length - 1;
            }
            this.location = JSON.parse(JSON.stringify(this.blockShape[this.rotation]));
            this.moveTo(this.x, this.y);
        }
        catch (err) {
            console.log('### ERROR');
            console.log(err);
            console.log(this.rotation);
            console.log(this.blockShape[this.rotation]);
            console.log(JSON.stringify(this.blockShape[this.rotation]));
        }
    }

    moveTo(x, y) {
        this.x = x;
        this.y = y;

        for (var i in this.location) {
            this.location[i].x += x;
            this.location[i].y += y;
        }
    }

    moveDown() {
        this.y++;
        for (var i in this.location) {
            this.location[i].y++;
        }
    }
    moveUp() {
        this.y--;
        for (var i in this.location) {
            this.location[i].y--;
        }
    }

    moveLeft() {
        this.move(-1);
    }
    moveRight() {
        this.move(1);
    }

    move(x) {
        this.x += x;
        for (var i in this.location) {
            this.location[i].x += x;
        }
    }

    close() {
        this.sprite.destroy();
    }

    getWidth() {
        var countBlocks = [];
        for (var i in this.location) {
            if (!countBlocks.includes(this.location[i].x)) {
                countBlocks.push(this.location[i].x);
            }
        }
        return countBlocks.length;
    }

    getHeight() {
        var countBlocks = [];
        for (var i in this.location) {
            if (!countBlocks.includes(this.location[i].y)) {
                countBlocks.push(this.location[i].y);
            }
        }
        return countBlocks.length;
    }
}
export default Piece;