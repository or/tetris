export default class Playfield {
    constructor(game, graphics, position, settings, blockWidth, nextPiecePreview, score) {
        this.game = game;
        this.graphics = graphics;
        this.position = position;
        this.width = settings.width;
        this.height = settings.height;
        this.backgroundColor = settings.backgroundColor;
        this.gridColor = settings.gridColor;
        this.blockWidth = blockWidth;
        this.nextPiecePreview = nextPiecePreview;
        this.score = score;

        this.initiateBoard();
        this.draw();
    }

    initiateBoard() {
        this.boardData = new Array(this.width);

        for (var x = 0; x < this.width; x++) {
            this.boardData[x] = new Array(this.height);
            for (var y = 0; y < this.height; y++) {
                this.boardData[x][y] = this.backgroundColor;
            }
        }
    }

    draw() {
        this.graphics.clear();
        var positionX = this.position.x;
        var positionY = this.position.y;

        for (var y = 0; y < this.height; y++) {
            for (var x = 0; x < this.width; x++) {
                var figure = new Phaser.Polygon([
                    new Phaser.Point(positionX, positionY),
                    new Phaser.Point(positionX + this.blockWidth, positionY),
                    new Phaser.Point(positionX + this.blockWidth, positionY + this.blockWidth),
                    new Phaser.Point(positionX, positionY + this.blockWidth)]);

                this.graphics.beginFill(this.boardData[x][y]);
                this.graphics.lineStyle(1, 0xdddddd, 1)
                this.graphics.drawPolygon(figure.points);
                this.graphics.endFill();

                positionX += this.blockWidth;
                if (positionX >= this.position.x + this.width * this.blockWidth) {
                    positionX = this.position.x;
                }
            }

            positionY += this.blockWidth;
            if (positionY >= this.position.y + this.height * this.blockWidth) {
                positionY = this.position.y;
            }
        }
    }

    spawnPiece() {
        var x = Math.floor(this.width / 2) - 1;
        this.currentPiece = this.nextPiecePreview.nextBlock;
        this.currentPiece.moveTo(x, 0);

        this.nextPiecePreview.newNextBlock();

        if (this.checkCollision(this.currentPiece)) {
            this.game.state.start("GameOver", false);
        }

        this.showBlock(this.currentPiece);
        this.draw();
    }

    clearBlock(block) {
        for (var i in block.location) {
            var location = block.location[i];
            if (location.x >= 0 && location.x < this.width && location.y >= 0 && location.y < this.height) {
                this.boardData[location.x][location.y] = this.backgroundColor;
            }
        }
    }

    clearLines() {
        var lines = [];
        for (var y = 0; y < this.height; y++) {
            var blocksInRow = 0;
            for (var x = 0; x < this.width; x++) {
                if (this.boardData[x][y] != this.backgroundColor) blocksInRow++;
            }

            if (blocksInRow == this.width) {
                lines.push(y);
            }
        }
        lines.sort();
        this.removeLines(lines);

        return lines.length;
    }

    removeLines(lines) {
        for (var line in lines) {
            for (var x = 0; x < this.width; x++) {
                this.boardData[x][lines[line]] = this.backgroundColor;

                for (var y = lines[line]; y > 0; y--) {
                    this.boardData[x][y] = this.boardData[x][y - 1];
                }
            }
        }
        this.draw();
    }

    showBlock(block) {
        for (var i in block.location) {
            this.boardData[block.location[i].x][block.location[i].y] = block.color;
        }
        this.draw();
    }

    checkCollision(block) {
        for (var i in block.location) {
            if (block.location[i].y >= this.height) return true;
            if (block.location[i].x < 0 || block.location[i].x >= this.width) return true;
            if (this.boardData[block.location[i].x][block.location[i].y] != this.backgroundColor) return true;
        }
        return false;
    }

    howManyBlocksCollideWithRightWall(block) {
        var collisions = [];
        for (var i in block.location) {
            if (block.location[i].x >= this.width) {
                if (!collisions.includes(block.location[i].x)) {
                    collisions.push(block.location[i].x);
                }
            }
        }
        return collisions.length;
    }

    close() {
        this.sprite.destroy();
    }

    rotateBlock() {
        this.clearBlock(this.currentPiece);
        this.currentPiece.rotate();

        var rightWallCollision = this.howManyBlocksCollideWithRightWall(this.currentPiece);
        if (rightWallCollision > 0) {
            this.currentPiece = this.moveBlock(this.currentPiece, -1 * rightWallCollision);
        }

        if (this.checkCollision(this.currentPiece)) {
            this.currentPiece.rotateBack();
        }

        this.showBlock(this.currentPiece);
        this.draw();
    }

    moveBlockRight() {
        this.moveCurrentBlock(1);
    }

    moveBlockLeft() {
        this.moveCurrentBlock(-1);
    }

    moveBlock(block, x) {
        this.clearBlock(block);
        block.move(x);

        if (this.checkCollision(block)) {
            block.move(-1 * x);
        }
        return block;
    }

    moveCurrentBlock(x) {
        this.clearBlock(this.currentPiece);
        this.currentPiece.move(x);

        if (this.checkCollision(this.currentPiece)) {
            this.currentPiece.move(-1 * x);
        }

        this.showBlock(this.currentPiece);
    }

    nextStep() {
        this.clearBlock(this.currentPiece);
        this.currentPiece.moveDown();

        if (this.checkCollision(this.currentPiece)) {
            this.currentPiece.moveUp();
            this.showBlock(this.currentPiece);
            this.score.calculate(this.clearLines());
            this.spawnPiece();
            return;
        }

        this.showBlock(this.currentPiece);
        this.draw();
    }

    moveDown() {
        this.nextStep();
        this.score.increment();
    }
}
export default Playfield;
