class Score {
    constructor(game, blockWidth, columnWidth, positionX, positionY, settings) {
        this.game = game;
        this.blockWidth = blockWidth;
        this.columnWidth = columnWidth;
        this.positionX = positionX;
        this.positionY = positionY;
        this.settings = settings;
        this.lineHeight = settings.lineHeight;

        this.setup();
    }

    setup() {
        this.currentScore = 0;
        this.highScore = window.localStorage.getItem("highScore");
        if (this.highScore === null) this.highScore = 0;

        this.addLabelText(this.settings.text.score, this.positionY);
        this.addLabelText(this.settings.text.highscore, this.positionY + (2 * this.lineHeight + 1) * this.blockWidth);

        this.currentScoreText = this.addValueText(this.currentScore, this.positionY + this.lineHeight * this.blockWidth);
        this.highScoreText = this.addValueText(this.highScore, this.positionY + (3 * this.lineHeight + 1) * this.blockWidth);
    }

    addLabelText(value, positionY) {
        const labelStyle = this.getTextStyle(this.settings.label);

        this.addText(
            value,
            labelStyle,
            this.positionX,
            positionY,
            this.columnWidth,
            this.lineHeight);
    }

    addValueText(value, positionY) {
        const valueStyle = this.getTextStyle(this.settings.value);

        return this.addText(
            value,
            valueStyle,
            this.positionX,
            positionY,
            this.columnWidth,
            this.lineHeight);
    }

    addText(value, style, x, y, width, height) {
        let text = this.game.add.text(0, 0, value, style);
        text.setTextBounds(x, y, width, height);
        return text;
    }

    getTextStyle(settings) {
        return {
            font: settings.font,
            fontSize: this.blockWidth + 'px',
            fill: settings.color,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        };
    }

    increment() {
        this.currentScore++;
    }

    calculate(lines) {
        switch (lines) {
            case 1:
                this.currentScore += 40;
                break;
            case 2:
                this.currentScore += 100;
                break;
            case 3:
                this.currentScore += 300;
                break;
            case 4:
                this.currentScore += 1200;
                break;
        }
    }

    update() {
        if (this.currentScore > this.highScore) {
            this.highScore = this.currentScore;
            window.localStorage.setItem("highScore", this.currentScore);
            this.highScoreText.setText(this.currentScore);
        }
        this.currentScoreText.setText(this.currentScore);
    }
}
export default Score;