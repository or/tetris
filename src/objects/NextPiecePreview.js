import Piece from 'objects/Piece';

export default class NextPiecePreview {
    constructor(graphics, position, blockWidth, settings, pieceTemplates) {
        this.graphics = graphics;
        this.position = position;
        this.blockWidth = blockWidth;
        this.settings = settings;
        this.pieceTemplates = pieceTemplates;

        this.defaultBoardColor = this.settings.backgroundColor;
        this.width = this.settings.width * this.blockWidth;
        this.height = this.settings.height * this.blockWidth;

        this.newNextBlock();
    }

    draw() {
        this.graphics.clear();
        var positionX = this.position.x;
        var positionY = this.position.y;

        var figure = new Phaser.Polygon([
            new Phaser.Point(positionX, positionY),
            new Phaser.Point(positionX + this.width, positionY),
            new Phaser.Point(positionX + this.width, positionY + this.height),
            new Phaser.Point(positionX, positionY + this.height)]);

        this.graphics.beginFill(this.defaultBoardColor);
        this.graphics.lineStyle(1, 0xdddddd, 1)
        this.graphics.drawPolygon(figure.points);
        this.graphics.endFill();
    }

    newNextBlock() {
        this.nextBlock = new Piece(this.pieceTemplates);

        var blockWidth = this.nextBlock.getWidth();
        var blockHeight = this.nextBlock.getHeight();

        this.blockData = new Array(blockWidth);
        for (var x = 0; x < blockWidth; x++) {
            this.blockData[x] = new Array(blockHeight);
            for (var y = 0; y < blockHeight; y++) {
                this.blockData[x][y] = this.defaultBoardColor;
            }
        }

        for (var i in this.nextBlock.location) {
            this.blockData[this.nextBlock.location[i].x][this.nextBlock.location[i].y] = this.nextBlock.color;
        }

        this.drawBlock();
    }

    drawBlock() {
        this.graphics.clear();
        this.draw();

        var blockWidth = this.nextBlock.getWidth();
        var blockHeight = this.nextBlock.getHeight();

        var centeredpositionX = this.position.x + (this.width - blockWidth * this.blockWidth) / 2;
        var centeredpositionY = this.position.y + (this.height - blockHeight * this.blockWidth) / 2;

        var positionX = centeredpositionX;
        var positionY = centeredpositionY;



        for (var y = 0; y < blockHeight; y++) {
            for (var x = 0; x < blockWidth; x++) {
                var figure = new Phaser.Polygon([
                    new Phaser.Point(positionX, positionY),
                    new Phaser.Point(positionX + this.blockWidth, positionY),
                    new Phaser.Point(positionX + this.blockWidth, positionY + this.blockWidth),
                    new Phaser.Point(positionX, positionY + this.blockWidth)]);

                if (this.blockData[x][y] != this.defaultBoardColor) {
                    this.graphics.beginFill(this.blockData[x][y]);
                    this.graphics.lineStyle(1, 0xdddddd, 1);
                    this.graphics.drawPolygon(figure.points);
                    this.graphics.endFill();
                }

                positionX += this.blockWidth;
                if (positionX >= centeredpositionX + blockWidth * this.blockWidth) {
                    positionX = centeredpositionX;
                }
            }

            positionY += this.blockWidth;
            if (positionY >= centeredpositionY + blockHeight * this.blockWidth) {
                positionY = centeredpositionY;
            }
        }
    }

}
export default NextPiecePreview;
