import Boot from 'states/Boot';
import Preload from 'states/Preload';
import MainMenu from 'states/MainMenu';
import Game from 'states/Game';
import GameOver from 'states/GameOver';



class Tetris extends Phaser.Game {

    constructor() {
        super(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.AUTO);

        this.state.add('Boot', Boot, false);
        this.state.add('Preload', Preload, false);
        this.state.add('MainMenu', MainMenu, false);
        this.state.add('Game', Game, false);
        this.state.add('GameOver', GameOver, false);

        this.state.start('Boot');
    }

}

new Tetris();
