class Preload extends Phaser.State {
    preload() {
        this.game.load.image('start', 'assets/btn-start.png');
        this.game.load.image('game-over', 'assets/btn-game-over.png');

        this.game.load.image('left', 'assets/left-arrow.png');
        this.game.load.image('right', 'assets/right-arrow.png');
        this.game.load.image('up', 'assets/up-arrow.png');
        this.game.load.image('down', 'assets/down-arrow.png');

        this.game.load.image('logo', 'assets/logo.png');

        this.game.load.json('shapes', 'assets/shapes.json');
        this.game.load.json('settings', 'assets/settings.json');

        
    }

    create() {
        this.game.state.start('MainMenu');
    }

}
export default Preload;