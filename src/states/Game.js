import NextPiecePreview from 'objects/NextPiecePreview';
import Playfield from 'objects/Playfield';
import Score from 'objects/Score';
import Controller from 'controllers/UI';


class Game extends Phaser.State {

    create() {
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        
        this.settings = this.game.cache.getJSON('settings');
        const pieceTemplates = this.game.cache.getJSON('shapes');

        const gameWidthInBlocks = this.settings.playfield.width
            + this.settings.nextPiecePreview.width
            + 2 * this.settings.game.padding    //right and left padding
            + 1;    //space between playfield and next piece preview

        this.game.blockWidth = Math.floor(this.game.world.width / gameWidthInBlocks);

        const rightColumnPositionX = (this.settings.game.padding + 1 + this.settings.playfield.width) * this.game.blockWidth;
        const rightColumnWidth = this.settings.nextPiecePreview.width * this.game.blockWidth;

        this.score = new Score(
            this.game,
            this.game.blockWidth,
            rightColumnWidth,
            rightColumnPositionX,
            (this.settings.nextPiecePreview.width + 2 * this.settings.game.padding) * this.game.blockWidth,
            this.settings.score);

        let nextPiecePreview = new NextPiecePreview(
            this.game.add.graphics(0, 0),
            new Phaser.Point(rightColumnPositionX, this.settings.game.padding * this.game.blockWidth),
            this.game.blockWidth,
            this.settings.nextPiecePreview,
            pieceTemplates
        );

        this.playfield = new Playfield(
            this.game,
            this.game.add.graphics(0, 0),
            new Phaser.Point(this.settings.game.padding * this.game.blockWidth, this.settings.game.padding * this.game.blockWidth),
            this.settings.playfield,
            this.game.blockWidth,
            nextPiecePreview,
            this.score);

        this.playfield.spawnPiece();
        this.controller = new Controller(this.game, this.playfield, this.game.blockWidth);
    }

    
    update() {
        this.controller.update();
        this.score.update();
    }
}
export default Game;