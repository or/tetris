class GameOver extends Phaser.State {

	create() {
		this.game.controls.forEach(function(x) {
	    x.pendingDestroy = true;
		});

		var buttonWidth = this.game.world.width / 2;
		var buttonHeight = buttonWidth / 3.125;

    var button = this.add.button(this.game.world.centerX - buttonWidth / 2, 24 * this.game.blockWidth, 'game-over', this.restartGame, this, 1, 0, 2);
		button.width = buttonWidth;
		button.height = buttonHeight;
	}

	restartGame() {
		this.game.state.start("MainMenu");
	}
}

export default GameOver;
