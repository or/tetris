class UI {
    constructor(game, playfield, blockWidth) {
        this.tickers = {
            controls: {
                interval: 10, // 6 times/s on 60FPS
                counters: {
                    ArrowLeft: 0,
                    ArrowRight: 0,
                    ArrowDown: 0
                }
            },
            game: {
                interval: 60, // 1 time/s on 60FPS
                counter: 0
            }
        };

        this.game = game;
        this.playfield = playfield;
        this.blockWidth = blockWidth;

        this.setupControls();
    }


    setupControls() {
        let leftButton = this.game.add.button(2 * this.blockWidth, 24 * this.blockWidth, 'left', this.releaseButtons, this, 1, 0, 2);
        let downButton = this.game.add.button(5 * this.blockWidth, 27 * this.blockWidth, 'down', this.releaseButtons, this, 1, 0, 2);
        let rightButton = this.game.add.button(8 * this.blockWidth, 24 * this.blockWidth, 'right', this.releaseButtons, this, 1, 0, 2);
        let upButton = this.game.add.button(14 * this.blockWidth, 24 * this.blockWidth, 'up', this.playfield.rotateBlock, this.playfield, 1, 0, 2);

        const buttonSize = 3 * this.blockWidth;

        this.setButtonSize(leftButton, buttonSize);
        this.setButtonSize(rightButton, buttonSize);
        this.setButtonSize(downButton, buttonSize);
        this.setButtonSize(upButton, buttonSize);

        leftButton.onInputDown.add(() => { this.buttonLeft = true; });
        rightButton.onInputDown.add(() => { this.buttonRight = true; });
        downButton.onInputDown.add(() => { this.buttonDown = true; });

        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.cursors.up.onDown.add(this.playfield.rotateBlock, this.playfield);

        this.game.controls = [leftButton, downButton, rightButton, upButton];
    }

    setButtonSize(button, size) {
        button.width = size;
        button.height = size;
    }

    releaseButtons() {
        this.buttonDown = false;
        this.buttonRight = false;
        this.buttonLeft = false;
    }

    

    update() {
        if (this.cursors.left.isDown || this.buttonLeft) {
            if (this.tickers.controls.counters.ArrowLeft % this.tickers.controls.interval === 0) {
                this.playfield.moveBlockLeft();
            }
            this.tickers.controls.counters.ArrowLeft++;
        } else {
            this.tickers.controls.counters.ArrowLeft = 0;
        }

        if (this.cursors.right.isDown || this.buttonRight) {
            if (this.tickers.controls.counters.ArrowRight % this.tickers.controls.interval === 0) {
                this.playfield.moveBlockRight();
            }
            this.tickers.controls.counters.ArrowRight++;
        } else {
            this.tickers.controls.counters.ArrowRight = 0;
        }

        if (this.cursors.down.isDown || this.buttonDown) {
            if (this.tickers.controls.counters.ArrowDown % this.tickers.controls.interval === 0) {
                this.playfield.moveDown();
            }
            this.tickers.controls.counters.ArrowDown++;
        } else {
            this.tickers.controls.counters.ArrowDown = 0;
        }

        if (this.tickers.game.counter % this.tickers.game.interval === 0) {
            this.playfield.nextStep();
            this.tickers.game.counter = 0;
        }

        this.tickers.game.counter++;
        
    }
}
export default UI;